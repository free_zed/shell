#!/bin/zsh

# nextcloud/manual-upgrade.sh :
# - Backup current data & config
# - follow manual upgrade steps (https://docs.nextcloud.com/server/stable/admin_manual/maintenance/manual_upgrade.html#upgrade-manually)

# Author: freezed <git@freezed.me> 2020-04-06
# Licence: GNU GPL v3 [www.gnu.org/licenses/gpl.html]
# Dependences: zsh, keychain & backupninja


# #################
# Local variables #
# #################
THIRD_PARTY_APPS=('calendar' 'contacts' 'tasks')
DB_BACKUP_PATH='/path/to/backup' # no trailing slash
WEB_ROOT_PATH='/path/to/webserver/root' # no trailing slash
OCC_CMD_PATH=${WEB_ROOT_PATH}'/nextcloud/occ'


# ################ #
# Getted variables #
# ################ #
CURRENT_VERSION=`sudo -u www-data php ${OCC_CMD_PATH} --version|sed -e 's/Nextcloud //'`
TARGET_VERSION=`sudo -u www-data php ${WEB_ROOT_PATH}/nextcloud/occ update:check|head -n 1|sed -e 's/Nextcloud //'|cut -d' ' -f1`
NC_TARGET_ARCHIVE='https://download.nextcloud.com/server/releases/nextcloud-'${TARGET_VERSION}'.zip'

# ###### #
# Script #
# ###### #
echo -e '\n\n\t\t##################################'
echo '> Upgrading Nextcloud from v'${CURRENT_VERSION}' to v'${TARGET_VERSION}

##  Maintenance mode
sudo -u www-data php ${OCC_CMD_PATH} maintenance:mode --on
wget $NC_TARGET_ARCHIVE --directory-prefix=/tmp

# Backup
# ######

##  Put some backup commands here before messing around
##  I personally use [backupninja](https://0xacab.org/riseuplabs/backupninja)

##  Ask SSH passphrase only one time
eval `keychain --eval --agents ssh id_rsa`

##  Backup
echo '> Starting Nextcloud backup DB in '${DB_BACKUP_PATH}' & data…'
backupninja --run /etc/backup.d/20-nextcloud-dumpx.sh
backupninja --run /etc/backup.d/30-nextcloud-rsync.sh

### ZSH nice trick for getting last modified file
### found on : https://stackoverflow.com/a/38996701/6709630
LATEST_DB_BAK_FILE=`printf '%s\n' ${DB_BACKUP_PATH}/*(.om[1])`

### Mark DB backup just made with version number
cp ${LATEST_DB_BAK_FILE} ${LATEST_DB_BAK_FILE%.sql.gz}-$CURRENT_VERSION.sql.gz
echo '> … done!'

## Nextclod commands
echo '> Disabling 3rd party apps…'
for app in ${THIRD_PARTY_APPS[*]};do
    echo -e '\t> '${app}
    sudo -u www-data php ${OCC_CMD_PATH} app:disable ${app}
done
echo '> … done!'

echo '> Moving current Nextcloud directory to '${WEB_ROOT_PATH}'/old-nextcloud-'${CURRENT_VERSION}'…'
service apache2 stop
echo -e '\t> Apache is stopped'
mv ${WEB_ROOT_PATH}/nextcloud ${WEB_ROOT_PATH}/old-nextcloud-${CURRENT_VERSION}

unzip -qq -d ${WEB_ROOT_PATH} /tmp/nextcloud-${TARGET_VERSION}.zip
echo -e '\t> Nextcloud archive is unzipped'
cp ${WEB_ROOT_PATH}/old-nextcloud-${CURRENT_VERSION}/config/config.php ${WEB_ROOT_PATH}/nextcloud/config/.

echo -e '\t> Copying 3rd party apps directory from '${CURRENT_VERSION}'…'
for app in ${THIRD_PARTY_APPS[*]};do
    echo -e '\t>\t> '${app}
    cp -r ${WEB_ROOT_PATH}/old-nextcloud-${CURRENT_VERSION}/apps/${app} ${WEB_ROOT_PATH}/nextcloud/apps/.
done

chown -R www-data:www-data ${WEB_ROOT_PATH}/nextcloud
find ${WEB_ROOT_PATH}/nextcloud/ -type d -exec chmod 750 {} \;
find ${WEB_ROOT_PATH}/nextcloud/ -type f -exec chmod 640 {} \;
echo -e '\t> Rights are set'
service apache2 start
echo -e '\t> Apache is started'
echo '> … done!'

echo '> Upgrading Nextcloud to '${TARGET_VERSION}'…'
sudo -u www-data php ${OCC_CMD_PATH} upgrade
echo '> … done!'

echo '> Enabling & upgrading 3rd party apps to fit '${TARGET_VERSION}'…'
for app in ${THIRD_PARTY_APPS[*]};do
    echo -e '\t> '${app}
    sudo -u www-data php ${OCC_CMD_PATH} app:enable ${app}
    sudo -u www-data php ${OCC_CMD_PATH} app:update ${app}
done
echo '> … done!'

sudo -u www-data php ${OCC_CMD_PATH} maintenance:mode --off
