#!/bin/bash
set -uo pipefail
IFS=$'\n\t'

# rename-pdf-file.sh:
# Rename PDF files base on PDF metadata
#

# Author: freezed <2160318-free_zed@users.noreply.gitlab.com > 2024-03-04
# Licence: [GNU AGPL v3](http://www.gnu.org/licenses/)
#

# Config
DEBUG="yes"
DEST_PATTERN="document*.pdf"
REQUIRED_COMMANDS="cut
pdftk
find
grep
mv
sed"

help()
{
   echo -e "Rename PDF files base on PDF metadata\n"
   echo -e "Syntax: ${0##*/} [-h] [-d] [-w]"
   echo -e "options:"
   echo -e "-h Print this help."
   echo -e "-d dry run"
   echo -e "-w write run\n"
}

# Log message to stderr as well as syslog
log() {
    if [[ "${DEBUG}" == "yes" ]]; then
        logger -s -p user.notice -- "NOTE: $@"
    fi
}

# Log warning to stderr as well as syslog
warn() {
    logger -s -p user.info -- "INFO: $@"
}

# Log error to stderr as well as syslog
err() {
    logger -s -p user.err -- "ERROR $@"
}

check_req_cmd()
{
    if ! which $REQUIRED_COMMANDS > /dev/null; then
        err "One or more of the required shell commands does is not available in your system"
        err "Required commands: $REQUIRED_COMMANDS"
        exit 1
    fi
}

file_process()
{
    check_req_cmd
    for file in $(find ${DEST_PATH} -type f -name ${DEST_PATTERN}); do
        PDF_METADATA=$(pdftk ${file} dump_data 2> /dev/null)

        while IFS= read -r line; do

            if [[ ${line} =~ "InfoValue: D:" ]]; then
                MAYBE_DATE=${line}
                log "will rename: ${file} -> ${DEST_PATH}${MAYBE_DATE:13:8}-cr-cse-${file#$DEST_PATH'document'}"

                if [[ "${WRITE_RUN}" == "yes" ]]; then
                   mv "${file}" "${DEST_PATH}${MAYBE_DATE:13:8}-cr-cse-${file#$DEST_PATH'document'}" && log "Done!"
                fi
                break
            fi
        done <<< "${PDF_METADATA}"
    done
}


# Parse option
while getopts "dwh" option; do
   case $option in
      d) WRITE_RUN="no" && file_process && exit 0 ;;
      w) WRITE_RUN="yes" && file_process && exit 0 ;;
      *) help && exit 0;;
   esac
done

[[ $# -eq 0 ]] && help || exit 0

