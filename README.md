My Shell Sandbox
----------------

A place for playing around with shell scripts.

Content
=======

* [`dico`](https://gitlab.com/free_zed/shell/-/tree/master/dico/)
    * [filter](https://gitlab.com/free_zed/shell/-/tree/master/dico/filtre.sh) a word list with regular expression
    * [count](https://gitlab.com/free_zed/shell/-/tree/master/dico/count_letters.sh) letters in a word list
* [`pdf`](https://gitlab.com/free_zed/shell/-/tree/master/pdf/)
    * Convert PDFs to PNG with [`pdftk`](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/), [`imagemagick`](https://www.imagemagick.org/)
* [`pip`](https://gitlab.com/free_zed/shell/-/tree/master/pip/)
    * list dependencies for a python package (using pip)
* [`nextcloud`](https://gitlab.com/free_zed/shell/-/tree/master/nextcloud/)
    * Automate a [manual Nextcloud upgrade](https://docs.nextcloud.com/server/stable/admin_manual/maintenance/manual_upgrade.html#upgrade-manually)
