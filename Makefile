EDITOR = geany

clean:
	# Remove files not in source control
	find . -type f -name "*.retry" -delete
	find . -type f -name "*.orig" -delete

open_all:
	${EDITOR} .gitignore Makefile README.md
	${EDITOR} ./dico/*
	${EDITOR} ./nextcloud/*
	${EDITOR} ./pdf/*
	${EDITOR} ./pip/*
